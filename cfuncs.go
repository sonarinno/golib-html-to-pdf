package html2pdf

/*
#cgo CFLAGS: -I./wkhtmltopdf/include
#cgo LDFLAGS: -lwkhtmltox -L./wkhtmltopdf/lib
#include <wkhtmltox/pdf.h>
#include <stdio.h>

void progress_changed_cgo(wkhtmltopdf_converter *converter, int p) {
  progress_changed_cgo(converter, p);
}

void phase_changed_cgo(wkhtmltopdf_converter *converter) {
  phase_changed_cgo(converter);
}

void set_error_cgo(wkhtmltopdf_converter *converter, const char *msg) {
  set_error_cgo(converter, msg);
}

void set_warning_cgo(wkhtmltopdf_converter *converter, const char *msg) {
  set_warning_cgo(converter, msg);
}

*/
import "C"
